import { v4 as uuidv4 } from "uuid";
import multer from "multer";
import path from "path";

export const staticDirectory = path.resolve(__dirname, "public");
export const uploadDirectory = path.resolve(staticDirectory, "uploads");

// const storage = multer.diskStorage({
//   destination: (req, file, callback) => {
//     callback(null, uploadDirectory);
//   },
//   filename: (req, file, callback) => {
//     const fileName = `${uuidv4()}${path.extname(file.originalname)}`;
//     callback(null, fileName);
//   },
// });
// export const uploader = multer({ storage });

export const uploader = multer({
  storage: multer.diskStorage({
    destination: (req, file, callback) => {
      callback(null, uploadDirectory);
    },
    filename: (req, file, callback) => {
      const fileName = `${uuidv4()}${path.extname(file.originalname)}`;
      callback(null, fileName);
    },
  }),
});

export const getId = () => uuidv4();

export const PORT_TO_USE = process.env.PORT || 4000;
