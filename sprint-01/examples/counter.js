// button to increment a count
// button to decrement a count
// show the current count
// if the count goes over a specific number play a sound and or change the color of the text

const getColor = (state, props) => {
  if (state.count < props.gettingClose) {
    return "green";
  }
  if (state.count >= props.stopAt) {
    return "red";
  }
  return "purple";
};

class Counter extends React.Component {
  state = {
    color: "green",
    count: 1,
    actualCount: 0,
    timesIncremented: 0,
    percentage: null,
  };

  // click1 = new Audio("/click1.wav");
  // click2 = new Audio("/click1.wav");

  audioFiles = {
    click1: new Audio("/click1.wav"),
    click2: new Audio("/click2.wav"),
  };

  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     name: "Vince",
  //     count: 0,
  //   };
  //   this.increment = this.increment.bind(this);
  //   this.decrement = this.decrement.bind(this);
  // }
  increment = () => {
    this.audioFiles.click1.play();
    //  DO NOT DO THIS EVER I WILL SCREAMMMMM AT YOU
    // this.setState({ count: this.state.count + 1})

    // NEVER DO THISSSSSSSSSS
    // this.state.count++
    // NEVER DO THISSSSSSSSSS
    // this.state.count += 1;

    // Do this =]
    this.setState((state, props) => {
      const nextState = {
        timesIncremented: state.timesIncremented + 1,
      };

      const actualCount = state.actualCount + 1,

      const pct = actualCount / nextState.timesIncremented;
      const percentage = Number.isNaN(pct) ? null : pct;

      if (state.count >= props.stopAt) {
        this.audioFiles.click2.play();
        return nextState;
      }
      return {
        ...nextState,
        actualCount,
        percentage,
        count: state.count * props.multiplier,
        color: getColor(state, props),
      };
    });
  };

  decrement = () => {
    this.setState((state, props) => {
      return {
        count: state.count - 1,
        color: getColor(state, props),
      };
    });
  };

  // getPercentage() {
  //   const { timesIncremented, actualCount } = this.state;
  //   const pct = actualCount / timesIncremented;
  //   const percentage =  Number.isNaN(pct) ? "" : `${pct}%`;
  //   return percentage ? `Shot Percentage: ${percentage}`: null;
  // }


  shouldNotWork = 0;

  wontWork = () => {
    this.shouldNotWork = this.shouldNotWork + 1;
  }

  render() {
    // console.log("Counter Re-rendered", this);
    const { color, count, percentage } = this.state;
    console.log("counter =]");
    return (
      <React.Fragment>
        <p
          style={{
            color,
          }}
        >
          {count}
        </p>
        <p>{this.shouldNotWork}</p>
        <button onClick={this.wontWork}>This wont work</button>
        <button onClick={this.increment}>Up</button>
        <button onClick={this.decrement}>Down</button>
        <img src="/download.jpg" />

        {percentage ? <p>Shot Percentage: {percentage}%</p> : null}
      </React.Fragment>
    );
  }
}
