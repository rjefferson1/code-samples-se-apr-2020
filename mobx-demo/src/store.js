import { makeAutoObservable } from "mobx";

class Store {
  counter = 0;
  todos = [
    {
      id: 1,
      name: "Grocery Shopping",
      completed: false,
    },
    {
      id: 2,
      name: "Get hair loc'd",
      completed: false,
    },
    {
      id: 3,
      name: "Talk Students to death",
      completed: true,
    },
  ];

  constructor() {
    makeAutoObservable(this);
  }

  increment = () => {
    this.counter++;
    console.log("increment called");
  };

  decrement = () => {
    this.counter--;
    console.log("decrement called");
  };

  get completedTodos() {
    return this.todos.filter((todo) => todo.completed);
  }

  toggleTodo = (id) => {
    this.todos.forEach((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
    });
  };
}

const store = new Store();

export default store;

window.store = store;
