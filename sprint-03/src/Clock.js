import React, { useEffect } from "react";
import useCounter from "./hooks/useCounter";
import useTimer from "./hooks/useTimer";

const Clock = () => {
  const date = useTimer();
  const counter = useCounter();

  useEffect(() => {
    console.log("Dj Khaled Effect");
    return () => {
      console.log("Another 1!!");
    };
  }, [counter.count]);

  return (
    <>
      <p>{date.toLocaleTimeString()}</p>
      <button onClick={counter.increment}>Say Somethin'</button>
      <p>{counter.count}</p>
    </>
  );
};

export default Clock;
