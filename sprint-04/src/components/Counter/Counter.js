import React from "react";

const Counter = ({ count, increment, decrement, multiply }) => {
  // console.log({ count, increment, decrement, multiply });
  return (
    <>
      <p>{count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <button onClick={multiply}>Multiply</button>
    </>
  );
};

export default Counter;
