class App extends React.Component {
  state = {
    counters: [
      {
        count: 0,
        image: "https://icatcare.org/app/uploads/2018/07/Thinking-of-getting-a-cat.png",
      },

      {
        count: 0,
        image:
          "https://media.wired.com/photos/5e1e646743940d0008009167/2:1/w_2298,h_1149,c_limit/Science_Cats-84873657.jpg",
      },
    ],
  };

  upOrDown = (direction = "up", index) => {
    return () => {
      this.setState((state) => {
        const { counters } = state;
        const nextCounters = counters.map((cntr, idx) => {
          if (idx === index) {
            return {
              ...cntr,
              count: direction === "up" ? cntr.count + 1 : cntr.count - 1,
            };
          }
          return cntr;
        });
        return {
          counters: nextCounters,
        };
      });
    };
  };

  render() {
    const { counters } = this.state;
    return (
      <React.Fragment>
        {counters.map((cntr, index) => (
          <CounterExample key={index} {...cntr} inc={this.upOrDown("up", index)} dec={this.upOrDown("down", index)} />
        ))}
        {/* <CounterExample count={count} inc={this.upOrDown("up")} dec={this.upOrDown("down")} /> */}
      </React.Fragment>
    );
  }
}

const CounterExample = (props) => {
  const { count, inc, dec, image } = props;
  return (
    <React.Fragment>
      {count}
      <button onClick={inc}>Up</button>
      <button onClick={dec}>Down</button>
      <img src={image} style={{ width: "100px" }} />
    </React.Fragment>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
