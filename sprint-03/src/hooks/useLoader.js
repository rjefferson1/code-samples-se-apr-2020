import { useState } from "react";

const useLoader = (initialValue = false) => {
  const [isLoading, setLoading] = useState(initialValue);

  const toggle = () => setLoading((val) => !val);

  return {
    isLoading,
    toggle,
    setLoading,
  };
};

export default useLoader;
