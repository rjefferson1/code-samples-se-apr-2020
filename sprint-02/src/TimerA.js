import React from "react";

// THIS IS APPLE
class TimerA extends React.Component {
  interval = null;

  componentDidMount() {
    this.props.sendMessage(`${this.props.name}: componentDidMount`);
    this.hollaAtYourBoy();
  }

  hollaAtYourBoy = () => {
    this.interval = setInterval(() => {
      this.props.sendMessage(`${this.props.name}: Sup bruh`);
    }, 2000);
  };

  componentWillUnmount() {
    this.props.sendMessage(`${this.props.name}: componentWillUnmount`);
    clearInterval(this.interval);
  }

  componentDidUpdate() {
    console.log(`${this.props.name}: componentDidUpdate`);
  }

  render() {
    const { name } = this.props;
    return (
      <>
        <p>{name}</p>
      </>
    );
  }
}

export default TimerA;
