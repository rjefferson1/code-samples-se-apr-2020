import mongoose from "mongoose";

const UserSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    default: "basic",
    enum: ["basic", "admin", "super-admin"],
  },
  likesFood: Boolean,
  pets: [{ name: String, age: Number }],
});

// Registers the "User" model with mongoose so it can be used anywhere
export const UserModel = mongoose.model("User", UserSchema);
