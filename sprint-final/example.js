import express from "express";

// express is a library that makes handling http(s)/tcp requests really simple
const app = express();

const nicoCoolMiddleware = (req, res, next) => {
  console.log("sup sup sup");
  next();
};

// app.use(nicoCoolMiddleware);

app.get(
  "/",
  nicoCoolMiddleware,
  nicoCoolMiddleware,
  nicoCoolMiddleware,
  nicoCoolMiddleware,
  nicoCoolMiddleware,
  (req, res, next) => {
    res.send("Hi family!!");
  },
);

// app.use(express.json()); // an example of something we would use

// tell the computer to listen on this port to any incoming requests
app.listen("3030", () => {
  console.log(`Server successfully running on port 3030`);
});
