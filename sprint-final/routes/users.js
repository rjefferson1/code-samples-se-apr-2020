import express from "express";
import { UserModel } from "../schemas";

export default function userController(app) {
  const router = express.Router();

  router.use((req, res, next) => {
    // this comes from the user's request
    const authHeader = req.headers.authorization;

    if (!authHeader || authHeader !== process.env.AUTH_TOKEN) {
      res.status(401).send("You dont belong");
      return;
    }
    next();
  });

  router.get("/", async (req, res) => {
    try {
      const users = await UserModel.find();
      res.send(users);
    } catch (err) {
      console.error(err);
      res.status(500).send("Internal Server Error");
    }
  });

  router.post("/", async (req, res) => {
    try {
      // validation here
      // if the validation fails
      // res.status(400).send("Bad data bruh")
      const user = await UserModel.create(req.body);
      res.status(201).send(user);
    } catch (err) {
      console.error(err);
      res.status(500).send("Internal Server Error");
    }
  });

  app.use("/users", router);
}
