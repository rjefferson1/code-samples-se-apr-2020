class App extends React.Component {
  render() {
    console.log("App Re-rendered");
    return (
      <React.Fragment>
        <Counter multiplier={2} stopAt={10} gettingClose={8} />
        <Counter multiplier={5} stopAt={20} gettingClose={10} />
      </React.Fragment>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));
